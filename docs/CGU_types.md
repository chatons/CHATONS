Ce document présente des CGU types, présentés par blocs selon les informations que vous voulez transmettre dans votre document. Il ne s’agit pas d’imposer un style ou de donner des règles précises mais plutôt de faciliter l’exercice d’écrire ce document, tout en se rassurant sur le fait qu’il ne manque rien d’important.

Les mots en majuscule représentent les noms à ajuster. Lorsque plusieurs choix sont possibles, ils sont encerclés dans un bloc de code.

Conditions Générales de Services
==============

DATE

En utilisant les services de NOM_CHATON, vous acceptez d’être lié par les conditions suivantes.

## Évolution des conditions générales de service
NOM_CHATON se réserve le droit de mettre à jour et modifier ces conditions. Dans ce cas, NOM_CHATON informe les personnes concernées par mail si elle le peut ou par un affichage sur le site.

> Archivage

Un historique daté des versions de cette charte peut être récupéré sur LIEN

## Accès aux services

> Accès libre
### Services en libre accès

Les services suivant sont en libre-accès :
ACCES_LIBRE

> Accès par compte

### Services accessibles via un compte
Les services suivant sont accessibles via un compte :
ACCES_PAR_COMPTE

>> Adhésion

Votre groupe ou vous même devez adhérer à l’association pour obtenir un compte sur ces services.

>> Abonnement

Par ailleurs, les services suivant nécessitent de s’acquitter d’un abonnement:
ABONNEMENT


### Approbation préalable
Les services accessibles par compte sont soumis à l’approbation préalable de NOM_CHATON selon les ressources disponibles.

>> Groupes

Par ailleurs, l’accès est restreint aux personnes de GROUPES

### Périmètre géographique
NOM_CHATON s’adresse avant tout aux personnes physique ou morale résidant ou ayant un lien avec le lieu suivant: PERIMETRE. Toutefois, il est possible d’utiliser les services et d’adhérer en dehors de ce périmètre si aucun service équivalent n’est disponible plus proche de chez-vous. 

## Fonctionnement
### Délais de mise en service

> Bénévoles

**NOM_CHATON ne s’engage sur aucun délai de mise en service.**

NOM_CHATON propose les services suivants grâce à des bénévoles, de ce fait NOM_CHATON ne s’engage sur aucun délai de mise en service. Nous essayons toutefois de faire de notre mieux pour les fournir dans les DELAIS_MISE_EN_SERVICE jours.

> Non Bénévoles

**NOM_CHATON met en service les applications commandées sous DELAIS_MISE_EN_SERVICE jours à partir de la réception des éléments nécessaires à leur déploiement.** Ce délai ne s’applique pas lors des congés annuels notifiés sur le site.


### Transmission sécurisée d’identifiants
**NOM_CHATON ne vous demandera jamais de communiquer vos mots ou phrases de passe**
NOM_CHATON ne vous demandera jamais de communiquer vos mots ou phrases de passe. Lorsque NOM_CHATON doit vous transmettre un identifiant NOM_CHATON le fera via METHODE_TRANSMISSION_MOT_DE_PASSE

### Intervention en cas de panne
**En cas de panne constatée et si aucun message n’atteste sur la page de statut que NOM_CHATON est en train de corriger le dysfonctionnement, vous devez faire un signalement via METHODE_TICKET.**

> Bénévoles

**Si ça casse, on fera de notre mieux MAIS nous ne sommes pas obligés de réparer**
NOM_CHATON propose l’ensemble de ces services grâce à des bénévoles qui feront ce qu’ils et elles peuvent pour résoudre les problèmes techniques qui pourraient subvenir.

> Abonnement + Adhésion

Si vous avez un abonnement, vous êtes également membre et à ce titre vous avez une part de responsabilité dans le bon fonctionnement de l’association. Si le service est indisponible ou si vos données sont perdus, ce sera donc une faute collective.

En cas d’incapacité à résoudre un problème technique, NOM_CHATON pourra prendre la décision de fermer le service.

Dès réception d’un signalement, NOM_CHATON intervient le plus promptement possible compte tenue du contexte (nombre d’usagers impactés, gravité, temps nécessaire pour résoudre le problème, disponibilité, etc).


### Responsabilité de NOM_CHATON

> Bénévoles

**En aucun cas, un ou une utilisatrice ne pourra se prévaloir de dommages ou indemnités résultant de problèmes techniques de quelque nature que ce soit.**

NOM_CHATON est assujetti à une obligation de moyens, considérant la haute technicité des technologies mises en œuvre. En cas de défaillance, NOM_CHATON ne peut être tenu pour responsable des dommages indirects tels que pertes d’exploitation, préjudices commerciaux, perte de Clientèle, de chiffre d’affaires, de bénéfices ou d’économies prévus, ou de tout autre préjudice indirect.

> Non Bénévoles

**NOM_CHATON est assujetti à une obligation de moyens, considérant la haute technicité des technologies mises en œuvre.** En cas de défaillance, NOM_CHATON ne peut être tenu pour responsable des dommages indirects tels que pertes d’exploitation, préjudices commerciaux, perte de Clientèle, de chiffre d’affaires, de bénéfices ou d’économies prévus, ou de tout autre préjudice indirect.

Une fois le système de sauvegarde en place, NOM_CHATON ne pourra pas être tenu responsable de :
 - la perte de données des dernières 24h induites par la restauration d’une sauvegarde
 - la perte ou divulgation de données du fait d’une erreur, d’une panne, d’une attaque ou de plusieurs de ces faits à la fois, survenant simultanément sur ou contre le serveur et/ou l’une des deux sauvegardes 

La responsabilité de NOM_CHATON ne saurait être engagée en cas de retards ou d’inexécutions résultant d’un cas de force majeure, telle que reconnue par la jurisprudence des tribunaux français. Sont également considérés comme des cas de force majeure au titre des présentes :
 - le blocage ou l’interruption des réseaux de télécommunications,
 - l’absence ou la suspension de la fourniture d’électricité par le fournisseur historique,
 - les catastrophes naturelles,
 - l’arrêt maladie (ou le congé parental soudain) de plus de 2 semaines de ___________,
 - et tout autre cas indépendant de la volonté du Prestataire et empêchant l’exécution normale de la prestation. 

### Mésusage des services
> Bénévoles

**NOM_CHATON n’est pas là pour vous couvrir et prendre des risques légaux à votre place, même si votre action est légitime, vous êtes entièrement responsables de ce que vous faites.**

> Ressources partagées

**La consommation de ressources doit respecter le cadre mutualisé de nos services. Tout abus peut entraîner la fermeture d’un compte.**

Vous devez respecter les lois et réglementations en vigueur lors de l’usage des services proposés que ce soit en matière de respect de la vie privée, d’envoi de mails en grande quantité, de propriété intellectuelle, de propos discriminatoires, d’appel à la haine, de harcèlement, d’atteinte aux libertés fondamentales de personnes, etc.

En cas d’usage prohibé, NOM_CHATON peut se trouver dans l’obligation de déclencher la suspension totale ou partielle du service, le retrait de contenu, ou toute autre mesure que les lois et réglementations lui imposent.

Vous devez respecter les autres utilisateurs en faisant preuve de civisme et de politesse. NOM_CHATONS se réserve le droit de supprimer tout contenu paraissant non pertinent ou contrevenant à ces principes, selon son seul jugement.

> Ressources partagées

Par ailleurs, si un ou une utilisatrice abuse du service, par exemple en monopolisant des ressources machines partagées, son contenu ou son accès pourra être supprimé, si nécessaire sans avertissement ni négociation. NOM_CHATON reste seul juge de cette notion « d’abus » dans le but de fournir le meilleur service possible à l’ensemble des usagers et usagères.

> Bénévoles
### Devenir des services
**Rien n’est éternel**
NOM_CHATON peut par ailleurs choisir (de résilier des abonnements ou) d’arrêter des services si NOM_CHATON estime ne plus être en mesure de fournir lesdits services. Si NOM_CHATON en a la possibilité, elle fera de son mieux pour laisser un délai suffisant pour permettre à tout le monde de migrer sereinement.

### Support et conseil
Vous devez nommer jusqu’à cinq personnes habilitées à communiquer avec NOM_CHATONS, dans le cadre de l’utilisation des services.

> Bénévoles

Vous pouvez nous signaler des souhaits sur la création de futur service, mais sachez que nous ne pourrons pas créer de nouveaux services en moins de 6 mois. NOM_CHATON pourra toutefois vous rediriger vers des chatons à même de répondre à vos demandes

> Support incident

En dehors de dysfonctionnement technique nous ne proposons pas de support d’aide à l’utilisation en plus de notre documentation. Veuillez éviter également de monopoliser l’équipe de support sur des demandes hors du cadre des services proposés.

> Support payant

En dehors de dysfonctionnement technique, NOM_CHATONS propose de vous aider dans la réalisation de votre projet avec les services de NOM_CHATONS.

NOM_CHATONS se réserve le droit de facturer le temps de support et de conseil dépassant l’offre initiale.

> Autre support

Nous acceptons tout type de demande de support et ferons de notre mieux pour vous aider dans la réalisation de votre projet avec les services de NOM_CHATONS.

> Ateliers

Nous proposons des ateliers d’aide à l’usage de nos services.


> Accès par compte

### Résiliation d’un compte
Si vous souhaitez résilier un compte, vous devez le signaler à NOM_CHATON.

Il est du devoir de la personne d’avertir NOM_CHATON de son souhait d’arrêter un compte (_si abonnement_) 15 jours avant la date de fin d’abonnement souhaité. Si les abonnements à résilier représentent plus de 100€ / mois le délais de pré-avis est porté à 3 mois (sauf décision contraire du CA autorisant un pré-avis exceptionnellement plus court).


## Nos engagements
**Respects de vos données personnelles, votre vie privée, votre propriété intellectuelle, vos droits ainsi que le principe de neutralité du net**

NOM_CHATON n’exploitera vos données personnelles que dans le cadre de ces 5 finalités:
 - fournir le service pour lesquels vous avez transmis vos données
 - produire d‘éventuelles statistiques anonymisées et agrégées
 - vous prévenir d’un changement important sur le service (panne, notification d’intrusion et de vol de données, changement d’interface, date d’arrêt du service...)
 - obtenir votre avis sur les services et l’action de l’association
 - vous inviter à participer à un évènement de NOM_CHATON


NOM_CHATON ne transmettra ni ne revendra vos données personnelles (votre vie privée nous tient - vraiment - à cœur). Votre contenu vous appartient tout autant, toutefois, nous vous encourageons à le publier sous licence libre si c’est pertinent.

**Insistance sur l’importance du respect des données personnelles**
Une modification du paragraphe précédente, contrairement au reste de la présente charte, ne peut se faire simplement par une simple notification. Si une telle modification devait survenir, elle :
 - Ne serait pas rétroactive
 - Demandera un accord explicite de votre part pour continuer à utiliser les services fournis par NOM_CHATON
 - Provoquera une révocation préalable à la modification auprès de tous les soutiens de NOM_CHATONS ayant à cœur les problématiques de respect de la vie privée.

### Charte CHATONS
**NOM_CHATON s’engage à respecter la charte du Collectif des Hébergeurs, Alternatifs, Transparents, Ouverts, Neutres et Solidaires dans le cadre de son activité d’hébergeur et de fourniture de services en ligne**

> CHATONS

NOM_CHATON est membre ce collectif

> Candidat CHATONS

NOM_CHATON est candidat à l’intégration au sein de ce collectif.


PRECISION_BEMOL_CHARTE

Plus d’information sur la charte C.H.A.T.O.N.S. : https://chatons.org/fr/charte

### Localisation des données

> Selon service

**La localisation de vos données dépend de chaque service et est précisée dans les conditions générales de services**

> Selon prestation

**La localisation de vos données dépend de chaque prestation et est précisée sur chaque devis et à votre demande**

> Autres cas

**Vos données sont à LOCALISATION**


### Devenir des données
Une fois le compte clôturé, NOM_CHATON peut procéder à la suppression des données (ou décider de les conserver jusqu’à 2 mois en cas de résiliation suite à un retard de paiement).

> Libre accès

Certains services en libre accès permettent de configurer la péremption des données, d’autres les conservent de façon permanentes, mais vous pouvez demander leur retrait si vous pouvez prouver que vous en êtes l’auteur⋅ices.

### Exercice de vos droits
Conformément à l’article 34 de la loi « Informatique et Libertés », vous pouvez exercer les droits suivant en envoyant un mail sur MAIL_DROIT :

 - droits d’accès, de rectification, d’effacement et d’opposition
 - droit à la limitation du traitement
 - droit à la portabilité des données
 - droit de ne pas faire l’objet d’une décision individuelle automatisée

### RGPD
Vous et NOM_CHATONS s’engagent à respecter la réglementation en vigueur applicable au traitement de données à caractère personnel et, en particulier, le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 applicable à compter du 25 mai 2018, dite RGPD.


## Conditions financières

### Tarifs
Pour les services payants, les prix sont mentionnés en euros sur le site.

> Franchise TVA

Les prix sont indiqués en franchise de TVA (art 293b du CGI).

### Modification des prix

> Garantie de prix

Le prix de votre abonnement est garantie pour les 4 prochaines années.

> Sans garantie

NOM_CHATON peut décider d’augmenter les prix des abonnements.

### Tacite reconduction

**Les abonnements sont sans engagements mais sont reconduits tacitement (ainsi que l’adhésion si vous avez un abonnement actif.)**

### Facturation
Les factures sont disponibles dans l’espace membre.

> Prorata

Les abonnements sont facturés au pro-rata des jours d’abonnements. Par exemple si un service est commencé le 15 avril, la facture pour la période du 15 au 30 avril sera le prix d’un mois divisé par 2. Nous procédons de la même façon pour la cloture.

> Autre

Tout mois entamé sera facturé.

> Adhésion

Attention: les adhésions annuelles ne sont pas facturées au pro rata.


### Retard de paiement
**Tout impayé ayant fait l’objet d’une relance depuis 30 jours ou plus entrainera la coupure du service.**

Sauf exception décidée par NOM_CHATON, tout impayé ayant fait l’objet d’une première relance depuis 30 jours ou plus entraînera la coupure et la résiliation du service, sans aucune indemnité ou dommage revendicable par l’abonné⋅e. Les sommes facturées non payées resteront dues.

> Gratuit

L’ensemble des services est fournie gratuitement. N’hésitez pas à soutenir NOM_CHATONS en faisant un don.


## Litige et juridiction compétente
Le droit applicable aux présentes est le droit français. En cas de différent, les parties recherchent une solution amiable. Si la démarche échoue, le litige sera tranché par le Tribunal de Grande Instance de VILLE_CHATON.

Le fait que l’usager ou NOM_CHATON ne se prévale pas à un moment donné de l’une des présentes conditions générales et/ou tolère un manquement par l’autre partie ne peut être interprété comme valant renonciation par l’usager ou NOM_CHATON à se prévaloir ultérieurement de ces conditions.

La nullité d’une des clauses de ces conditions en application d’une loi, d’une réglementation ou d’une décision de justice n’implique pas la nullité de l’ensemble des autres clauses. Par ailleurs l’esprit général de la clause sera à conserver en accord avec le droit applicable.
