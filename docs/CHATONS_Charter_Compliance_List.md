# CHATONS Charter Compliance List

CHATONS – kittens in french – is the Collective of Hosters Alternative, Transparent, Open, Neutral and Solidarity. This collective aims to bring together structures offering free, ethical and decentralised online services.

This compliance list is a support to help the CHATONS who are in charge of giving an opinion on an application. This list remains optional and does not replace the appreciation of the CHATONS. It is above all there to help the candidates and also to allow the CHATONS to save time in the evaluation of the applications.


## Legend

❗ Important criteria that could potentially be a blocker for your application

ℹ️ Suggestions

The determination of whether a criterion is blocking or not is at the discretion of each member.


## Preliminary checks

- [ ] ❗ The website is online from the moment the application is posted.

- [ ] ❗ As part of the application process, at least one service is testable and accessible by the members of the collective. Access to a "demo" account satisfies this constraint if the services are not already in free access.

- [ ] ❗ The organization indicates that it respects the charter and does not suggest that it is already a member of the collective (it may, however, indicate that it is a candidate).

ℹ️ If the service portal is a sub-domain (e.g.: `services.mykitten.com`):
- Check the parent domain (`mykitten.com`) as well as `www.mykitten.com`.
- If the domains are used, make sure that they are not in obvious contradiction with the Charter.  
- Check the presence of trackers and the conformity of the site to the legislation of its country (legal mentions). 
- If the domains are unused, suggest a redirection (DNS records `CNAME`) to the services subdomain. 
     

## Advertising / trackers / third party calls

- [ ] ❗ No third party trackers are present.  
*This is checked with uBlock Origin, Privacy Badger and/or NoScript by disabling Firefox's protections as well as any PiHole. Third-party fonts and images are considered potential trackers.*
- [ ] ❗ If statistics are made, they do not require prior consent from users (https://www.cnil.fr/en/cookies-and-other-tracking-devices-cnil-publishes-new-guidelines).
- [ ] ℹ️ If the CHATON is forced to communicate via a centralized or private social network, the information can be accessed through another channel.

## Security

- [ ] ❗ Users have access to the main information about the security policy.  
*If you process personal data, this point is mandatory according to the GDPR*.

### Test the platform's websites with [Mozilla Observatory](https://observatory.mozilla.org/)

- [ ] ❗ The websites and the services of the CHATONS **are available via HTTPS**.
- [ ] ℹ️ Websites via HTTP do redirect to HTTPS.
- [ ] ℹ️ Websites are rated B or higher.  
*If not, suggest applying the security recommendations for web services https://ssl-config.mozilla.org/* .

### Test the SMTP servers of the platform with [CryptCheck](https://tls.imirhil.fr/) (verifiable with `dig MX mykitten.com`)  
- [ ] ℹ️ Mail servers are rated D or higher.  
*If not, suggest applying the security recommendations for SMTP*.

### Adminsys commitment
- [ ] ℹ️ People who have admin access, volunteers or employees commit to a charter regarding points of confidentiality and caution.  
*TODO: how is a person integrated into the CHATONS to do adminsys operations? What is the security/inclusivity trade-off?*

### Advanced security audit (to be done with the agreement of the future CHATONS?)
- [ ] ℹ️ Advanced: The other domains of the platform do not seem to contain backdoors (open adminer instances), etc.  
*The TLS certificate used by the applicant's website can possibly help to reveal these subdomains. The Let's Encrypt publication list also.*
- [ ] ℹ️ Advanced: There are no critical ports left open inadvertently.  
*To check this we can use `nmap`.*
- [ ] ℹ️ Advanced: The SSH port has been changed.
- [ ] ℹ️ Advanced: A ban mechanism is in place.  
*If we make 10 wrong password attempts somewhere, can we continue?*

Precautions to be taken should be discussed directly with the members of the applicant structure (prioritize public key authentication, forge a good passphrase, disk encryption, access management policy, etc).


## Backups

- [ ] ❗ Backup information is listed on the applicant's website (not just in the application).
- [ ] ℹ️ Frequency of backups, number of copies, extent of backups, techniques and software used are reliable.
- [ ] ❗ If the backup policy presents an unusual risk(s), users are properly informed of the limitations and risks.


## Providers and data localisation.

- [ ] ❗ The site indicates where data is hosted (including backups).
- [ ] ❗ The site indicates the supplier(s)/subcontractor(s) of the future CHATONS.
- [ ] ❗ The site indicates the degree of control the CHATONS has over its infrastructure.  
*Reminder: the CHATONS agrees to publicly and unambiguously display its level of control over the hardware and software hosting the services and associated data.*
- [ ] ❗ Future CHATONS' vendor(s)/subcontractor(s) are not inconsistent with the charter: 
    - Verify that the jurisdiction applied to the hosting of _each_ server of the structure allows it to comply with the Charter.
    - Ensure, to the extent possible, that the structure is not subject to the Cloud Act and other U.S. surveillance laws.
    - ℹ️ Use `whois` on the IP and/or domain of the website to verify the location of the structure.
    - Are the structure's servers subject to US jurisdiction, or a jurisdiction other than the structure's?
    - Are the servers owned by a foreign company? If they are owned by a US company, they are subject to the Cloud Act.
    - Can we locate each server used by the structure?
- [ ] ❗ If they are, the information is clearly indicated on the future CHATONS' website.  
*Including online payment service provider.*
- [ ] ❗ If the CHATON depends on a third party to host its services (server rental, etc.), the structure is transparent about the contractual provisions concerning the protection of hosted data.
- [ ] ℹ️ In case of renting servers (virtual or dedicated), the provider contractually commits to not accessing the data.


## Documentation

These informations are on the future CHATONS' website, possibly in a "Documentation" section.
- [ ] ❗ The documentation details the global infrastructure.
    - Level of control over the infrastructure (root access?).
    - Topology of the infrastructure.
    - Form of the infrastructure (VPS, web hosting, dedicated server?).
    - Hardware characteristics (processor, RAM).
    - Operating system installed on its system(s) and its version, virtualization technologies used if any...
    - ℹ️ List of installed packages.
- [ ] ❗ The documentation informs about the processes required to deploy each service.
- [ ] ℹ️ CHATONS' recurring procedures are documented (possibly in a private part).  
*Example: how to make backups, statistics, how to create an account, a vps to a new person...*
- [ ] ℹ️ A document or paragraph describing the security practices and measures to protect the data and infrastructure of the future CHATONS exists.
- [ ] ℹ️ A document on log retention times exists.


## Free and Open Source Software

- [ ] ❗ The Services operate using free software only.
*In the sense of the Free Software Foundation, whether or not they are compatible with the GPL license.In exceptional cases, the CHATON may offer a service based on a proprietary software or platform if no FLOSS solution is available to enable the service to operate in an acceptable manner.*
    - [ ] ❗ If the CHATON has to rely on proprietary software or platforms, it lists them on its website and justifies their use.
- [ ] ❗ On the website, there is a link to the source code of each service.
- [ ] ❗ the CHATON is offering a means to follow its news outside proprietary platforms (via its website, for example).
- [ ] ❗ the CHATON is offering a means of contact without having to create an account on a third-party platform.
- [ ] ❗🔍 the CHATONS is committed to use open formats when communicating with its beneficiaries.
- [ ] ℹ️ the CHATON works towards the emergence of FLOSS solutions for the proprietary software it uses.


## GTU & Legal  
**Please note: the GTU and the legal notice are two separate texts.**

### CGTC, GTU, or internal rules and regulations
- [ ] ❗ The **General Terms of Use / GTU** are accessible from the future CHATONS' website.
- [ ] ❗ The GTUs are clear and understandable, within the reach of anyone (not only lawyers).
- [ ] ℹ️ A short/condensed version is proposed in order to facilitate the understanding of the text.
- [ ] ❗ The GTU does not exclude a priori potential users to the proposed services. The CHATONS may, however, define "target audiences" to which it wishes to address itself (e.g. on criteria of geographical proximity or other interests). However, it must, as far as possible, respond to irrelevant requests, for example by directing them to another CHATONS or by facilitating the emergence of structures that would meet unmet needs.
- [ ] ❗ There is a "Personal Data and Privacy" clause clearly stating what is the CHATONS' policy regarding the practices covered.
- [ ] ❗ The rates for all offerings are publicly described (possibly with a link or by referring to the site).
- [ ] ❗ The future CHATONS undertakes in its TOS[GTU] not to arrogate to itself any ownership rights to the content, data and metadata produced by the hosted or the users.
- [ ] ❗ The GTU allow users to obtain the definitive deletion of any information (accounts and personal data) concerning the hosted, within the limits of legal and technical obligations.
- [ ] ❗ The GTU provides for the possibility for hosted users to leave the services by recovering the associated data in open formats, where possible.

### Legal notice
*In accordance with the [law](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000002457442&cidTexte=LEGITEXT000005789847&dateTexte=20190207)* : 

- [ ] ℹ️ The Legal Notice is accessible from the future CHATONS' website.
- [ ] ℹ️ The Legal Notice contains:
    - The name of the publication director (in the case of an organization, the legal name of the legal entity).
    - The name, first name, address and telephone number of the host.
    - If necessary, information regarding intellectual property rights (licensing of publications) and the storage policy of cookies and personal data.

## Human Contacts

- [ ] ❗ The site allows all hosted people to communicate with the future CHATONS by highlighting at least one way to do so.  
*Contact page, phone number, email, mailing list, forum, support ticket, etc. Caution: regarding some means the public must be able to easily make contact.*
- [ ] ❗ It is possible to physically meet, exchange, and participate in the future CHATONS.
- [ ] ❗ Exchanges with other CHATONS are benevolent.
- [ ] ❗ There are no blatantly malicious practices toward users in the GTU.
- [ ] ℹ️ There are no testimonials describing malicious behavior on the part of the future CHATONS.


## Accessibility
A good source of information: https://developer.mozilla.org/en-US/docs/Web/Accessibility

### A11y & CHATONS website.
- [ ] ℹ️ The role of elements corresponds well to what they visually represent.  
*For example a ``<a>`` tag transformed into a button must contain the attribute ```role="button"``. Title tags should be used rather than enlarging the font of a ``<span>``*.
- [ ] ℹ️ The colors of the site are sufficiently contrasted so as not to discriminate against the visually impaired.  
*The [contrast measure](https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Perceivable/Color_contrast) built into Firefox's developer tools can be helpful.  
The free [ColorOracle](https://colororacle.org/) tool can also be useful.
- [ ] ℹ️ Images needed to understand the page have alternative text with the `alt` attribute, decorative only images contain `alt=""`.
- [ ] ℹ️ The page accepts that users can override styles.  
*A style sheet is used instead of `style=""` attributes allowing to override the style (colorblind, visually impaired)*.
- [ ] ℹ️ Suggestion: When accessing services, information on the accessibility of the service is given.  
*This could be an icon with a tooltip*.

### A11y & CHATONS services
- [ ] ℹ️ The CHATONS makes accessibility Merge Requests on the services implemented.

### Mobile Accessibility
- [ ] ℹ️ The site is minimally *responsive* (readable on phone) with no navigation issues.

### ADSL accessibility < 2Mbps
- [ ] ℹ️ Website elements are light enough.  
*Pushed to the extreme: https://solar.lowtechmagazine.com/fr/2018/09/how-to-build-a-lowtech-website.html* .

## Appearance, clarity and finish.

- [ ] ℹ️ If the platform is aimed at the general public, the overall look and feel is good enough that it won't be a turn-off.
- [ ] ℹ️ There are no dead links.
- [ ] ℹ️ The structure has its own logo and visual identity.
- [ ] ❗ If the service is open without registration, the service is accessible via a link from the CHATONS' website.
- [ ] ❗ If the service is fee-based or registration-based, the terms of registration are clear.
- [ ] ℹ️ A page or paragraph describing each service is present, with a possible tutorial to guide users.  
*Provide the applicant with UI/UX tips that can improve the design of the services and/or the site.*


## Appendix: Application Form
*No field is required, fill in what you can*: 

- Name of the future CHATONS : ______________________
- Name of the parent structure if applicable: ______________________
*Example: the CHATON Chapril for the association April, or Bastet for Parinux.
- Legal form of the structure: ______________________  
*individual, association, company...*
- Site of the future CHATONS : https://______________________ 
- Website of the parent structure, if any : https://______________________ 
- Legal notice : https://______________________ 
- Terms of use : https://______________________ 
- Incident and maintenance log : https://______________________
- Documentation : https://______________________
- Backup details : https://______________________  
*Link to the elements that explain how backups are made (including: frequency of backups, number of copies, extent of backups, technique and software used)*.
- List of administrative requests: https://______________________  
*Details on copyright requests: [Framablog](https://framablog.org/2020/04/19/la-reponse-de-lhebergeur-a-la-bergere/), [Forum](https://forum.chatons.org/t/signalement-de-contenus-par-un-editeur/1028/10)*.
- Activity reports: https://______________________  
*These reports can include a description of your hosting activity, but also popular education or other outreach efforts.
- List of proprietary microcodes (if any): https://______________________
- Link to CHATONS' hosting information: ______________________
- Example of a contribution to open source:
\__________________________________________________________________
\__________________________________________________________________  
*This can be merge requests, translations, documentations, articles, donations, financial subscriptions to publishers, organization of events, install party*
- Example of a popular education approach:
\__________________________________________________________________
\__________________________________________________________________

The future CHATONS declares to respect the charter in all the required points and declares in particular:  
*This is a list of points that cannot be easily verified by the collective.
- [ ] have sufficient technical control over its infrastructure to guarantee data confidentiality and security (root access? encryption? trusted third party?)
- [ ] to give priority to the fundamental freedoms of its users, including the respect of their privacy, in all its actions;
- [ ] not to make any commercial use of the data or metadata of the hosted;
- [ ] not to carry out any monitoring of users' actions, other than for administrative, technical or internal service improvement purposes;
- [ ] not to practice any a priori censorship of the contents of the hosted;
- [ ] not to respond to administrative requests or authority requests requiring the communication of personal information before being presented with a legal request in due form;
- [ ] to apply its GTU with benevolence;
- [ ] to implement and promote an inclusive form of organization and governance that accommodates differences and values diversity by ensuring that marginalized or excluded groups are included in development processes;
- [ ] have an economic model based on solidarity;
- [ ] to offer reasonable prices in line with the costs of implementation;
- [ ] that the lowest salary (full-time equivalent, including bonuses and dividends) in the facility should not be less than one-quarter of the highest salary (full-time equivalent, including bonuses and dividends) in the facility.

Possible clarifications if the CHATONS is unsure of full compliance with the charter:
\__________________________________________________________________ 
\__________________________________________________________________ 
\__________________________________________________________________ 
\__________________________________________________________________ 
\__________________________________________________________________ 

