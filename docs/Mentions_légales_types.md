Ce document présente les informations importantes à mettre dans les mentions légales en fonction du type de structure du CHATONS


Mentions légales
================

 Tous les sites internet édités à titre professionnel, qu’ils proposent des ventes en ligne ou non, doivent obligatoirement indiquer les mentions légales suivantes :

* pour un entrepreneur individuel : nom, prénom, domicile
* pour une société : raison sociale, forme juridique, adresse de l’établissement ou du siège social (et non pas une simple boîte postale), montant du capital social adresse de courrier électronique et numéro de téléphone
* pour une activité commerciale : numéro d’inscription au registre du commerce et des sociétés (RCS)
* pour une activité artisanale : numéro d’immatriculation au répertoire des métiers (RM)
* en cas d’activité commerciale : numéro individuel d’identification fiscale numéro de TVA intracommunautaire
* pour une profession réglementée : référence aux règles professionnelles applicables et au titre professionnel nom et adresse de l’autorité ayant délivré l’autorisation d’exercer quand celle-ci est nécessaire
* nom du responsable de la publication
* coordonnées de l’hébergeur du site : nom, dénomination ou raison sociale, adresse et numéro de téléphone
* pour un site marchand, conditions générales de vente (CGV) : prix (exprimé en euros et TTC), frais et date de livraison, modalité de paiement, service après-vente, droit de rétractation, durée de l’offre, coût de la technique de communication à distance
* numéro de déclaration simplifiée Cnil, dans le cas de collecte de données sur les clients (non obligatoire, mais recommandé).

Avant de déposer ou lire un cookie, les éditeurs de sites ou d’applications doivent :

* informer les internautes de la finalité des cookies,
* obtenir leur consentement,
* fournir aux internautes un moyen de les refuser.

*À préciser*
La durée de validité de ce consentement est de 13 mois. Certains cookies sont cependant dispensés du recueil de ce consentement.

*À préciser*
On peut être exempté du recueil du consentement préalable des utilisateurs si on est en conformité avec l’[article 32-II de la loi du 6 janvier 1978](https://www.legifrance.gouv.fr/affichCnil.do?oldAction=rechExpCnil&id=CNILTEXT000028434058&fastReqId=811915411&fastPos=1), modifié par l’ordonnance n°2011-1012 du 24 août 2011 (transposé dans la directive 2009/136/CE). 

Le manquement à l’une de ces obligations peut être sanctionné jusqu’à un an d’emprisonnement, 75 000 € d’amende pour les personnes physiques et 375 000 € pour les personnes morales. 

